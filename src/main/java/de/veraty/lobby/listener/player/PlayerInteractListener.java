package de.veraty.lobby.listener.player;

import de.veraty.lobby.LobbyPlugin;
import de.veraty.lobby.objects.tools.Interactable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class PlayerInteractListener implements Listener {

    @EventHandler
    public void onCall(PlayerInteractEvent event) {
        if (event.getAction() != Action.PHYSICAL) {

            if (event.getItem() != null) {

                ItemStack itemStack = event.getItem();

                if (itemStack.hasItemMeta() && itemStack.getItemMeta().hasDisplayName()) {

                    Interactable interactable = LobbyPlugin.getInstance().getInteractable(itemStack);
                    if (interactable != null) {
                        interactable.onUse(event);
                        event.setCancelled(true);
                    }

                }

            }

        } else {

            //Physical

        }
    }

}
