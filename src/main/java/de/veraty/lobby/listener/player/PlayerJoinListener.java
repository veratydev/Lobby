package de.veraty.lobby.listener.player;

import de.veraty.lobby.LobbyPlugin;
import de.veraty.lobby.objects.user.User;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener implements Listener {

    @EventHandler
    public void onCall(PlayerJoinEvent event) {
        event.setJoinMessage("");

        User user = User.getInstance(event.getPlayer());
        user.flush();
        user.updateInventory();
        user.updateVisbility();

        if (LobbyPlugin.getInstance().getLobbyConfig().isChatClearActive()) {
            for (int i = 0; i < 50; i++) {
                event.getPlayer().sendRawMessage(" ");
            }
        }

        for (String welcomeMessage : LobbyPlugin.getInstance().getLobbyConfig().getJoinMessage()) {
            event.getPlayer().sendMessage(welcomeMessage);
        }

    }

}
