package de.veraty.lobby.listener.player;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;

public class PlayerKickListener implements Listener {

    @EventHandler
    public void onCall(PlayerKickEvent event) {
        event.setLeaveMessage(null);
        
        PlayerLeaveListener.onLeave(event.getPlayer());
    }

}
