package de.veraty.lobby.listener.player;

import de.veraty.lobby.objects.user.User;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerToggleFlightEvent;

public class PlayerToggleFlightListener implements Listener {

    @EventHandler
    public void onCall(PlayerToggleFlightEvent event) {
        User user = User.getInstance(event.getPlayer());
        if (!user.isBuildingAllowed()) {
            Location location = event.getPlayer().getLocation().subtract(0, 2, 0);

            if (location.getBlock().getType().isSolid()) {
                event.setCancelled(true);
                user.pushForward(0.3f, 0.5f);
            }

        }
    }

}
