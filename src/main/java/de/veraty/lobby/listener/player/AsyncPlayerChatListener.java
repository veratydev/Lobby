package de.veraty.lobby.listener.player;

import de.veraty.lobby.LobbyPlugin;
import de.veraty.lobby.objects.user.User;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class AsyncPlayerChatListener implements Listener {

    @EventHandler
    public void onCall(AsyncPlayerChatEvent event) {
        User user = User.getInstance(event.getPlayer());
        String message = event.getMessage();
        LobbyPlugin.broadcastChatMessage(LobbyPlugin.getInstance().getLobbyConfig().getChatFormat(), user.getPlayer().getDisplayName(), message);
        event.setCancelled(true);
    }

}
