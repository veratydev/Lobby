package de.veraty.lobby.listener.player;

import de.veraty.lobby.objects.user.User;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

public class PlayerDropItemListener implements Listener {

    @EventHandler
    public void onCall(PlayerDropItemEvent event) {
        User user = User.getInstance(event.getPlayer());
        if (!user.isBuildingAllowed()) {
            event.setCancelled(true);
        }
    }

}
