package de.veraty.lobby.listener.player;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitListener implements Listener {

    @EventHandler
    public void onCall(PlayerQuitEvent event) {
        event.setQuitMessage(null);

        PlayerLeaveListener.onLeave(event.getPlayer());
    }

}
