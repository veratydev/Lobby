package de.veraty.lobby.listener.block;

import de.veraty.lobby.objects.user.User;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class BlockBreakListener implements Listener {

    @EventHandler
    public void onCall(BlockBreakEvent event) {

        User user = User.getInstance(event.getPlayer());

        if (user.isBuildingAllowed()) {
            event.setCancelled(false);
        } else {
            event.setCancelled(true);
        }

    }

}
