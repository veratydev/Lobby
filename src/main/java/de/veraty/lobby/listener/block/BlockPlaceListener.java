package de.veraty.lobby.listener.block;

import de.veraty.lobby.objects.user.User;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

public class BlockPlaceListener implements Listener {

    @EventHandler
    public void onCall(BlockPlaceEvent event) {

        User user = User.getInstance(event.getPlayer());

        if (user.isBuildingAllowed()) {
            event.setCancelled(false);
        } else {
            event.setCancelled(true);
        }

    }

}
