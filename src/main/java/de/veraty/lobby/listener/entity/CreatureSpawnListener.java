package de.veraty.lobby.listener.entity;

import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;

import java.util.Arrays;
import java.util.List;

public class CreatureSpawnListener implements Listener {

    private List<CreatureSpawnEvent.SpawnReason> allowedSpawnReasons;

    public CreatureSpawnListener() {
        allowedSpawnReasons = Arrays.asList(CreatureSpawnEvent.SpawnReason.BUILD_IRONGOLEM, CreatureSpawnEvent.SpawnReason.BUILD_SNOWMAN, CreatureSpawnEvent.SpawnReason.CUSTOM);
    }

    @EventHandler
    public void onCall(CreatureSpawnEvent event) {
        if (allowedSpawnReasons.contains(event.getSpawnReason()) || event.getEntity() instanceof ArmorStand || event.getEntity() instanceof Projectile) {
            event.setCancelled(false);
        } else {
            event.setCancelled(true);
        }
    }

}
