package de.veraty.lobby.listener.entity;

import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;

public class EntitySpawnListener implements Listener {

    @EventHandler
    public void onCall(EntitySpawnEvent event) {
        if (event.getEntity() instanceof Item) {
            event.setCancelled(true);
        }
    }

}
