package de.veraty.lobby.listener.entity;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class PlayerDeathListener implements Listener {

    @EventHandler
    public void onCall(PlayerDeathEvent event) {

        event.setKeepLevel(true);
        event.setKeepInventory(true);

        event.setDroppedExp(0);
        event.getDrops().clear();

        event.getEntity().setHealth(20);

        event.setDeathMessage("");
    }

}
