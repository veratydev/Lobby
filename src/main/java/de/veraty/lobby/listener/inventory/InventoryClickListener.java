package de.veraty.lobby.listener.inventory;

import de.veraty.lobby.LobbyPlugin;
import de.veraty.lobby.objects.tools.Interactable;
import de.veraty.lobby.objects.tools.InventoryOpener;
import de.veraty.lobby.objects.user.User;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class InventoryClickListener implements Listener {

    @EventHandler
    public void onCall(InventoryClickEvent event) {

        User user = User.getInstance((Player) event.getWhoClicked());

        if (event.getClickedInventory() != null && event.getCurrentItem() != null) {
            Inventory inventory = event.getClickedInventory();
            ItemStack itemStack = event.getCurrentItem();

            if (itemStack.hasItemMeta() && itemStack.getItemMeta().hasDisplayName()) {
                if (inventory.getType() == InventoryType.PLAYER) {

                    Interactable interactable = LobbyPlugin.getInstance().getInteractable(itemStack);
                    if (interactable != null) {
                        interactable.onUse(new PlayerInteractEvent((Player) event.getWhoClicked(), Action.RIGHT_CLICK_AIR, itemStack, null, null));
                        event.setCancelled(true);
                    }

                } else {

                    InventoryOpener inventoryOpener = LobbyPlugin.getInstance().getInventoryOpener(inventory);
                    if (inventoryOpener != null) {
                        inventoryOpener.onClick(event);
                        event.setCancelled(true);
                    }

                }
            }
        }

        if (!user.isBuildingAllowed()) {
            event.setCancelled(true);
        }

    }

}
