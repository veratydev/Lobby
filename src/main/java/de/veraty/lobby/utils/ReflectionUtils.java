package de.veraty.lobby.utils;

import com.mojang.authlib.GameProfile;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class ReflectionUtils {

    private static String serverVersion = null, bukkitUri = null;

    private static Class<?> packetClass;

    public static Class<?> getPacketClass() throws ClassNotFoundException {
        if (packetClass == null) {
            packetClass = getCraftBukkitClass("Packet");
        }
        return packetClass;
    }

    public static String getServerVersion(Server server) {
        if (serverVersion == null) {
            String packageName = server.getClass().getPackage().getName();
            serverVersion = packageName.substring(packageName.lastIndexOf('.') + 1);
            bukkitUri = "net.minecraft.server." + serverVersion + ".";
        }
        return serverVersion;
    }

    public static Class<?> getCraftBukkitClass(String uri) throws ClassNotFoundException {
        return Class.forName(bukkitUri + uri);
    }

    public static Object getCommandMap(Server server) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        return server.getClass().getDeclaredMethod("getCommandMap").invoke(server);
    }

    public static Object getActionBarPacket(String actionBar) {
        try {
            Class<?> chatSerializerClass = getCraftBukkitClass("IChatBaseComponent");
            for (Class subClass : chatSerializerClass.getClasses()) {
                if (subClass.getSimpleName().equalsIgnoreCase("ChatSerializer")) {
                    Object baseComponent = subClass.getDeclaredMethod("a", String.class).invoke(null, actionBar);
                    Class<?> packetPlayOutChatClass = getCraftBukkitClass("PacketPlayOutChat");
                    return packetPlayOutChatClass.getDeclaredConstructor(chatSerializerClass, byte.class).newInstance(baseComponent, (byte) 2);
                }
            }
        } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException exception) {
            exception.printStackTrace();
        }
        return null;
    }

    public static SkullMeta getSkullMeta(String displayName, GameProfile gameProfile) {
        try {
            ItemStack itemStack = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
            SkullMeta skullMeta = (SkullMeta) itemStack.getItemMeta();
            skullMeta.setDisplayName(displayName);
            Field gameProfileField = skullMeta.getClass().getDeclaredField("profile");
            gameProfileField.setAccessible(true);
            gameProfileField.set(skullMeta, gameProfile);
            return skullMeta;
        } catch (NoSuchFieldException | IllegalAccessException exception) {
            exception.printStackTrace();
        }
        return null;
    }

    public static GameProfile getGameProfile(Player player) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        return (GameProfile) player.getClass().getDeclaredMethod("getProfile").invoke(player);
    }

    public static Object getPlayerConnection(Player player) {
        try {
            Object entityPlayerObject = player.getClass().getDeclaredMethod("getHandle").invoke(player);
            return entityPlayerObject.getClass().getDeclaredField("playerConnection").get(entityPlayerObject);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | NoSuchFieldException exception) {
            exception.printStackTrace();
        }
        return null;
    }

}
