package de.veraty.lobby.objects.chat;


public class ChatFormatter {


    public static String
            PREFIX = "§lUndefined >>",
            TEXT = "§<",
            HIGHLIGHT = "§>";


    public static String format(String string, Object... args) {
        return String.format(format(string), args);
    }

    public static String format(String string) {
        string = string
                .replace("§<", HIGHLIGHT)
                .replace("§>", TEXT);

        return PREFIX + " " + TEXT + string;
    }


}
