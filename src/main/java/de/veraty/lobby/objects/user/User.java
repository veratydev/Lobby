package de.veraty.lobby.objects.user;

import com.mojang.authlib.GameProfile;
import de.veraty.lobby.LobbyPlugin;
import de.veraty.lobby.objects.chat.ChatFormatter;
import de.veraty.lobby.objects.modules.gadgets.Gadget;
import de.veraty.lobby.objects.modules.tool.impl.ProfileTool;
import de.veraty.lobby.objects.modules.tool.impl.VanishTool;
import de.veraty.lobby.utils.ReflectionUtils;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.util.Vector;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Getter
@Setter
public class User {

    private volatile static Map<UUID, User> instances;

    static {
        instances = new HashMap<>();
    }

    private Player player;

    //props
    private boolean buildingAllowed;
    private boolean silentLobby;
    private VanishTool.VisibilityLevel visibilityLevel;
    private Gadget equippedGadget;
    private ItemStack skullItem, profileItem;

    //Reflect
    private Object playerConnection;

    public User(Player player) {
        this.player = player;
        this.silentLobby = false;
        this.visibilityLevel = VanishTool.VisibilityLevel.ALL;
        onPlayerUpdate();

        GameProfile gameProfile = null;
        try {
            gameProfile = ReflectionUtils.getGameProfile(player);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException exception) {
            exception.printStackTrace();
            gameProfile = new GameProfile(player.getUniqueId(), player.getName());
        }

        skullItem = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
        SkullMeta skullItemMeta = ReflectionUtils.getSkullMeta("§r§l" + player.getDisplayName(), gameProfile);
        skullItem.setItemMeta(skullItemMeta);

        profileItem = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
        SkullMeta profileItemMeta = ReflectionUtils.getSkullMeta(ProfileTool.DISPLAY_NAME, gameProfile);
        profileItem.setItemMeta(profileItemMeta);

    }

    public void showActionBar(String bar, Object... args) {
        String json = String.format("{ text: \"%s\" }", ChatFormatter.format(bar, args));
        sendPacket(ReflectionUtils.getActionBarPacket(json));
    }

    public void tell(String message, Object... args) {
        player.sendMessage(ChatFormatter.format(message, args));
    }

    public void sendPacket(Object packet) {
        if (packet != null && playerConnection != null) {
            try {
                playerConnection.getClass().getDeclaredMethod("sendPacket", ReflectionUtils.getPacketClass()).invoke(playerConnection, packet);
            } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InvocationTargetException exception) {
                exception.printStackTrace();
            }
        }
    }

    public void setPlayer(Player player) {
        if (this.player == null || this.player != player) {
            onPlayerUpdate();
        }
        this.player = player;
    }

    public void onPlayerUpdate() {
        this.playerConnection = ReflectionUtils.getPlayerConnection(player);
    }

    public void updateInventory() {
        if (!isBuildingAllowed()) {
            LobbyPlugin.getInstance().getLobbyConfig().getInventoryLoadout().fillHotbar(player);
        }
    }

    public void pushForward(float multiplier, float y) {
        player.setVelocity(player.getEyeLocation().getDirection().multiply(multiplier).setY(y));
    }

    public void push(float x, float y, float z) {
        player.setVelocity(player.getVelocity().add(new Vector(x, y, z)));
    }

    public void flush() {

        GameMode gameMode = isBuildingAllowed() ? GameMode.CREATIVE : GameMode.SURVIVAL;
        player.setGameMode(gameMode);
        player.getInventory().clear();

        double maxHealth = LobbyPlugin.getInstance().getLobbyConfig().getDefaultHealth();
        player.setHealth(maxHealth);
        player.setMaxHealth(maxHealth);

        for (PotionEffect potionEffect : player.getActivePotionEffects()) {
            player.removePotionEffect(potionEffect.getType());
        }

        player.setFoodLevel(20);
        player.setExp(0);
        player.setLevel(LobbyPlugin.getInstance().getLobbyConfig().getDefaultLevel());

    }

    public void updateVisbility() {
        for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
            User user = User.getInstance(onlinePlayer);
            updateVisbility(user);
        }
    }

    public void updateVisbility(User user) {
        if (user.canSee(this)) {
            user.player.showPlayer(this.player);
        } else {
            user.player.hidePlayer(this.player);
        }

        if (canSee(user)) {
            this.player.showPlayer(user.player);
        } else {
            this.player.hidePlayer(user.player);
        }

    }

    public boolean canSee(User user) {
        if (isSilentLobby() || user.isSilentLobby()) {
            return false;
        } else {
            return visibilityLevel.canBeSeen(user.getPlayer());
        }
    }

    public void play(Sound sound) {
        player.playSound(player.getEyeLocation().subtract(player.getEyeLocation().getDirection()), sound, 1, 1);
    }

    public static User getInstance(Player player) {
        if (instances.containsKey(player.getUniqueId())) {
            User user = instances.get(player.getUniqueId());
            user.setPlayer(player);
            return user;
        }

        User user = new User(player);
        instances.put(player.getUniqueId(), user);
        return user;
    }

}
