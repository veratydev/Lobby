package de.veraty.lobby.objects.tools;

import de.veraty.lobby.LobbyPlugin;
import de.veraty.lobby.objects.user.User;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

import java.util.List;

public interface InventoryOpener {

    Inventory getInventory(User user);

    boolean matches(Inventory other);

    void onClick(InventoryClickEvent event);


    default int toSlot(int[] point) {
        if (point.length == 1)
            return point[0];
        if (point.length == 2) {
            return point[0] * 9 + point[1];
        }
        return 0;
    }

    default void registerOpener() {
        List<InventoryOpener> inventoryOpeners = LobbyPlugin.getInstance().getInventoryOpeners();
        if (!inventoryOpeners.contains(this)) {
            inventoryOpeners.add(this);
        }
    }


}
