package de.veraty.lobby.objects.tools;

import java.io.File;
import java.io.IOException;

public class FileBuilder {

    private String directory, filename;

    public FileBuilder directory(String directory) {
        this.directory = directory;
        return this;
    }

    public FileBuilder filename(String filename) {
        this.filename = filename;
        return this;
    }

    public File build() {
        if (directory != null && filename != null) {
            File file = new File(directory, filename);

            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }

            try {
                if (!file.exists()) {
                    file.createNewFile();
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            }

            return file;
        }
        return null;
    }

}
