package de.veraty.lobby.objects.tools;

import de.veraty.lobby.LobbyPlugin;
import de.veraty.lobby.objects.user.User;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public interface Interactable {

    ItemStack getItemStack(User user);

    long getCooldown();

    boolean matches(ItemStack other);

    void onUse(PlayerInteractEvent event);

    default void registerInteractable() {
        List<Interactable> interactables = LobbyPlugin.getInstance().getInteractables();
        if (!interactables.contains(this)) {
            interactables.add(this);
        }
    }

}
