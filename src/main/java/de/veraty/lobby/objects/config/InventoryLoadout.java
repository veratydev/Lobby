package de.veraty.lobby.objects.config;

import de.veraty.lobby.objects.modules.tool.Tool;
import de.veraty.lobby.objects.user.User;
import lombok.Getter;
import org.bukkit.entity.Player;

@Getter
public class InventoryLoadout {

    private Tool[] hotbar;

    public InventoryLoadout() {
        hotbar = new Tool[9];
    }

    public void addTool(Tool tool) {
        if (tool != null) {
            if (tool.getSlot() < 9 && tool.getSlot() >= 0) {
                hotbar[tool.getSlot()] = tool;
            }
        }
    }

    public void removeTool(Tool tool) {
        if (tool != null) {
            if (tool.getSlot() < 9 && tool.getSlot() >= 0) {
                hotbar[tool.getSlot()] = null;
            }
        }
    }

    public boolean isSlotFree(int slot) {
        if (slot < 9 && slot >= 0) {
            return hotbar[slot] == null;
        }
        return false;
    }

    public void clear() {
        for (int i = 0; i < 9; i++) {
            hotbar[i] = null;
        }
    }

    public void fillHotbar(Player player) {
        User user = User.getInstance(player);
        for (int i = 0; i < hotbar.length; i++) {
            Tool tool = hotbar[i];
            if (tool != null) {
                if (tool.isPermitted(player)) {
                    player.getInventory().setItem(i, tool.getItemStack(user));
                }
            }
        }
    }

}
