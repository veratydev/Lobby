package de.veraty.lobby.objects.config;

import de.veraty.lobby.objects.chat.ChatFormatter;
import de.veraty.lobby.objects.config.ping.ServerPinger;
import de.veraty.lobby.objects.modules.tool.impl.*;
import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Getter
public class LobbyConfig {

    public static final String PERM = "lobby", PERM_COMMAND = PERM + ".command", PERM_TOOL = PERM + ".tool";

    private InventoryLoadout inventoryLoadout;

    private List<String> joinMessage;
    //Messages
    private String noPlayerError, noPermissionsError, notOnlineError, chatFormat;

    //Settings
    private int serverRefreshPeriod;
    private ServerPinger serverPinger;
    private boolean chatClearActive;

    private double defaultHealth;
    private int defaultLevel;


    public LobbyConfig() {
        this.inventoryLoadout = new InventoryLoadout();

        this.serverRefreshPeriod = 5 * 1000;
        this.serverPinger = new ServerPinger(new ArrayList<>());
    }

    public void load(FileConfiguration configuration) {

        readTools(configuration);

        List<String> rawJoinMessage = getElement(configuration, "message.join", new ArrayList<>());
        this.joinMessage = new ArrayList<>();

        for (String string : rawJoinMessage) {
            joinMessage.add(ChatColor.translateAlternateColorCodes('&', string));
        }

        this.chatFormat = "§f%s §8»§7 %s";

        ChatFormatter.PREFIX = getElement(configuration, "message.prefix", "§b§lLobby §3»");
        ChatFormatter.TEXT = getElement(configuration, "message.color", "§r");
        ChatFormatter.HIGHLIGHT = getElement(configuration, "message.highlight", "§6");

        noPermissionsError = ChatFormatter.format(getElement(configuration, "message.player.nopermissions", "Du hast dazu keine Berechtigung"));
        notOnlineError = ChatFormatter.format(getElement(configuration, "message.player.notonline", "Dieser Spieler ist nicht online"));

        noPlayerError = ChatFormatter.format(getElement(configuration, "message.player.noplayer", "Die Console darf diesen Befehl nicht ausführen"));

        defaultLevel = Calendar.getInstance().get(Calendar.YEAR);
        defaultHealth = 6;
        chatClearActive = true;
    }

    private void readTools(FileConfiguration configuration) {
        inventoryLoadout.addTool(new TeleporterTool());
        inventoryLoadout.addTool(new VanishTool());
        inventoryLoadout.addTool(new SilentHubTool());
        inventoryLoadout.addTool(new GadgetTool());
        inventoryLoadout.addTool(new NickTool());
        inventoryLoadout.addTool(new LobbySwitcherTool());
        inventoryLoadout.addTool(new ProfileTool());

    }

    private <E> E getElement(FileConfiguration configuration, String key, E def) {
        if (configuration.contains(key)) {
            Object object = configuration.get(key);

            if (object instanceof String) {
                object = ChatColor.translateAlternateColorCodes('&', (String) object);
            }

            return (E) object;
        }
        return def;
    }


}
