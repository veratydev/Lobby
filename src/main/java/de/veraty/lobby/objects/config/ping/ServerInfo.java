package de.veraty.lobby.objects.config.ping;

import lombok.Getter;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

@Getter
public class ServerInfo {

    private String name, hostname, joinPermission;
    private InetSocketAddress inetSocketAddress;
    private boolean online;
    private int port, players, maxPlayers;
    private Socket socket;

    public ServerInfo(String name, String hostname, String joinPermission, int materialId, int port) {
        this.name = name;
        this.hostname = hostname;
        this.joinPermission = joinPermission;
        this.port = port;
        this.inetSocketAddress = new InetSocketAddress(hostname, port);
        this.socket = new Socket();

        this.online = false;
        this.players = 0;
        this.maxPlayers = 0;
    }

    public void ping() throws IOException {

        try {
            socket.connect(inetSocketAddress);
        } catch (IOException exception) {
            online = false;
            throw exception;
        }

        OutputStream outputStream = socket.getOutputStream();
        InputStream inputStream = socket.getInputStream();
        outputStream.write(0xFE); //254

        int b = 0;
        StringBuffer str = new StringBuffer();
        while ((b = inputStream.read()) != -1) {
            if (b != 0 && b > 16 && b != 255 && b != 23 && b != 24) {
                str.append((char) b);
            }
        }

        String[] data = str.toString().split("§");
        data[0] = data[0].substring(1, data[0].length());

        try {
            this.players = Integer.parseInt(data[1]);
            this.maxPlayers = Integer.parseInt(data[2]);
        } catch (Exception exception) {
        } //Dont throw (parse)

        this.online = true;

        socket.close();
    }

}
