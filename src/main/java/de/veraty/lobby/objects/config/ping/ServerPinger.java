package de.veraty.lobby.objects.config.ping;

import de.veraty.lobby.LobbyPlugin;
import de.veraty.lobby.objects.tools.InventoryOpener;
import de.veraty.lobby.objects.tools.ItemBuilder;
import de.veraty.lobby.objects.user.User;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

@Getter
public class ServerPinger implements InventoryOpener {

    private volatile List<ServerInfo> serverInfos;
    private volatile Inventory inventory;
    private volatile long lastRefresh;
    private BukkitTask bukkitTask;

    public ServerPinger(List<ServerInfo> serverInfos) {
        this.serverInfos = serverInfos;
        this.inventory = Bukkit.createInventory(null, 9 * 5, "§r§bServer Wechsler");
        this.fillInventory();
    }

    public synchronized void run() {
        bukkitTask = new BukkitRunnable() {
            @Override
            public void run() {
                if (Bukkit.getOnlinePlayers().size() > 0) {
                    refresh();
                }
            }
        }.runTaskTimerAsynchronously(LobbyPlugin.getInstance(), 0, LobbyPlugin.getInstance().getLobbyConfig().getServerRefreshPeriod());
    }

    public synchronized void refresh() {

        Queue<ServerInfo> serverInfoQueue = new LinkedList<>(serverInfos);

        new BukkitRunnable() {
            @Override
            public void run() {
                while (!serverInfoQueue.isEmpty()) {
                    ServerInfo serverInfo = serverInfoQueue.remove();
                    try {
                        serverInfo.ping();
                    } catch (IOException exception) {
                        exception.printStackTrace();
                    }
                }

                lastRefresh = System.currentTimeMillis();
                fillInventory();
            }
        }.runTaskAsynchronously(LobbyPlugin.getInstance());

    }

    public void fillInventory() {

        inventory.setItem(toSlot(new int[]{2, 4}), new ItemBuilder().setMaterial(Material.BARRIER).setName("§cDieses Feature ist in der Entwicklung").build());

    }

    public void stop() {
        if (isRunning()) {
            bukkitTask.cancel();
            bukkitTask = null;
        }
    }

    public boolean isRunning() {
        return bukkitTask != null;
    }

    @Override
    public Inventory getInventory(User user) {
        return inventory;
    }

    @Override
    public boolean matches(Inventory other) {
        return other.getTitle().equalsIgnoreCase(inventory.getTitle());
    }

    @Override
    public void onClick(InventoryClickEvent event) {

    }
}
