package de.veraty.lobby.objects.modules;

import de.veraty.lobby.objects.modules.gadgets.Gadget;
import de.veraty.lobby.objects.modules.tool.Tool;
import de.veraty.lobby.objects.modules.warp.Warp;

import java.util.List;

public interface Module {

    void onEnable();

    void onDisable();

    void onUpdate();

    List<Module> getDependencies();

    List<Tool> getTools();

    List<Gadget> getGadgets();

    List<Warp> getWarps();

    String getName();

    int getVersion();

}
