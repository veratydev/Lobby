package de.veraty.lobby.objects.modules.warp;

import de.veraty.lobby.objects.tools.ItemBuilder;
import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;


@Getter
public class SimpleWarp implements Warp {

    private ItemStack itemStack;
    private String permission;
    private Location location;

    public SimpleWarp(String name, String permission, Location location, int itemId) {
        this.permission = permission;
        this.location = location;

        Material material = Material.getMaterial(itemId);

        this.itemStack = new ItemBuilder().setMaterial(material).setName(ChatColor.translateAlternateColorCodes('&', name)).build();
    }

    @Override
    public boolean isPermitted(Player player) {
        return player.hasPermission(permission);
    }
}
