package de.veraty.lobby.objects.modules;

import de.veraty.lobby.LobbyPlugin;
import de.veraty.lobby.objects.modules.gadgets.Gadget;
import de.veraty.lobby.objects.modules.tool.Tool;
import de.veraty.lobby.objects.modules.warp.Warp;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public abstract class LobbyModule implements Module {

    private List<Module> dependencies;
    private List<Tool> tools;
    private List<Gadget> gadgets;
    private List<Warp> warps;

    private String name;

    public LobbyModule(String name) {
        this.name = name;
        this.dependencies = new ArrayList<>();
        this.tools = new ArrayList<>();
        this.gadgets = new ArrayList<>();
        this.warps = new ArrayList<>();
    }

    @Override
    public void onEnable() {

    }

    @Override
    public void onDisable() {

    }

    @Override
    public void onUpdate() {

    }

    @Override
    public int getVersion() {
        return LobbyPlugin.getVersion();
    }
}
