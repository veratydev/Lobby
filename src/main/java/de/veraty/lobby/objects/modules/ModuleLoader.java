package de.veraty.lobby.objects.modules;

import de.veraty.lobby.LobbyPlugin;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class ModuleLoader {
    private List<Module> modules;
    private File directory;

    public ModuleLoader(File directory) {
        if (directory.isDirectory()) {
            this.modules = new ArrayList<>();
            this.directory = directory;
        }
    }

    @SuppressWarnings ({"resource", "deprecation", "unchecked"})
    public void loadModules()
            throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException {

        File[] jarFiles = directory.listFiles(new FileFilter() {

            @Override
            public boolean accept(File pathname) {
                return pathname.getName().endsWith(".jar");
            }

        });

        for (File file : jarFiles) {

            JarFile jarFile = new JarFile(file);
            for (Enumeration<JarEntry> entries = jarFile.entries(); entries.hasMoreElements(); ) {
                JarEntry entry = entries.nextElement();
                String name = entry.getName();

                if (name.endsWith(".class")) {
                    String className = name.replaceAll("/", ".").substring(0, name.length() - ".class".length());
                    URLClassLoader loader = URLClassLoader.newInstance(new URL[]{file.toURL()});

                    Class<?> clazz = loader.loadClass(className);
                    if (!clazz.isInterface()) {
                        for (Class<?> parent : clazz.getInterfaces()) {
                            if (parent.getName().equals(Module.class.getName())) {
                                loadModule((Class<Module>) clazz);
                            }
                        }
                    }
                }
            }
        }

        if (modules.size() > 0) {
            if (modules.size() == 1) {

            }
        }
        System.out.println(String.format("Loaded %d Module(s)", modules.size()));

    }

    public void loadModule(Class<Module> moduleClass) throws InstantiationException, IllegalAccessException {
        Module module = moduleClass.newInstance();
        modules.add(module);
    }

    public void enable() {
        List<Module> enabledModules = new ArrayList<>();
        for (Module module : modules) {
            if (module.getVersion() == LobbyPlugin.getVersion()) {
                System.out.println(String.format("Das Module %s ist nicht in der passenden Version",
                        module.getName()));
                enable(module, enabledModules);
            }
        }
    }

    private void enable(Module module, List<Module> enabledModules) {

        for (Module dependency : module.getDependencies()) {
            enable(dependency, enabledModules);
        }

        if (!enabledModules.contains(module)) {
            module.onEnable();

            LobbyPlugin.getInstance().getRegisteredTools().addAll(module.getTools());
            LobbyPlugin.getInstance().getWarps().addAll(module.getWarps());

            enabledModules.add(module);
        }
    }

    public void disable() {
        for (Module module : modules) {
            module.onDisable();
        }
    }

    public List<Module> getModules() {
        return modules;
    }

}
