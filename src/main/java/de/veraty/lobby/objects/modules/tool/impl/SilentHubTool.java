package de.veraty.lobby.objects.modules.tool.impl;

import de.veraty.lobby.objects.config.LobbyConfig;
import de.veraty.lobby.objects.modules.tool.LobbyTool;
import de.veraty.lobby.objects.tools.ItemBuilder;
import de.veraty.lobby.objects.user.User;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

public class SilentHubTool extends LobbyTool {

    private ItemStack silentHubActive, silentHub;

    public SilentHubTool() {
        super("silenthub", LobbyConfig.PERM_TOOL + ".silenthub", 2, 1000);

        this.silentHubActive = new ItemBuilder()
                .setMaterial(Material.TNT)
                .setName("§aSilentHub §8» §7Rechtsklick")
                .addEnchantment(Enchantment.ARROW_INFINITE, 1)
                .addFlag(ItemFlag.HIDE_ENCHANTS).build();

        this.silentHub = new ItemBuilder()
                .setMaterial(Material.TNT)
                .setName("§cSilentHub §8» §7Rechtsklick")
                .build();

        registerInteractable();
    }

    @Override
    public ItemStack getItemStack(User user) {
        if (user.isSilentLobby()) {
            return silentHubActive;
        } else {
            return silentHub;
        }
    }

    @Override
    public void onUse(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR) {
            User user = User.getInstance(event.getPlayer());
            if (user.isSilentLobby()) {
                user.setSilentLobby(false);
                user.play(Sound.DIG_GRAVEL);
                user.showActionBar("Du hast den §c§lSilentHub§> deaktiviert");
            } else {
                user.setSilentLobby(true);
                user.play(Sound.NOTE_PLING);
                user.showActionBar("Du hast den §a§lSilentHub§> aktiviert");
            }

            user.updateVisbility();
            user.getPlayer().getInventory().setItem(getSlot(), getItemStack(user));
        }
    }

    @Override
    public boolean matches(ItemStack other) {
        return other.getItemMeta().getDisplayName().contains("SilentHub");
    }
}
