package de.veraty.lobby.objects.modules.tool.impl;

import de.veraty.lobby.LobbyPlugin;
import de.veraty.lobby.objects.config.LobbyConfig;
import de.veraty.lobby.objects.modules.tool.LobbyTool;
import de.veraty.lobby.objects.tools.ItemBuilder;
import de.veraty.lobby.objects.user.User;
import org.bukkit.Material;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class LobbySwitcherTool extends LobbyTool {

    private ItemStack itemStack;

    public LobbySwitcherTool() {
        super("lobbyswitcher", LobbyConfig.PERM_TOOL + ".lobbyswitcher", 7, 500);
        this.itemStack = new ItemBuilder().setMaterial(Material.NETHER_STAR).setName("§bLobbywechsler §8» §7Rechtsklick").build();
    }

    public ItemStack getItemStack(User user) {
        return itemStack;
    }

    @Override
    public boolean matches(ItemStack other) {
        return itemStack.getItemMeta().getDisplayName().equalsIgnoreCase(other.getItemMeta().getDisplayName());
    }

    @Override
    public void onUse(PlayerInteractEvent event) {
        event.getPlayer().openInventory(LobbyPlugin.getInstance().getLobbyConfig().getServerPinger().getInventory());
    }
}
