package de.veraty.lobby.objects.modules.tool.impl;

import de.veraty.lobby.objects.config.LobbyConfig;
import de.veraty.lobby.objects.modules.tool.CommandTool;
import de.veraty.lobby.objects.tools.ItemBuilder;
import org.bukkit.Material;

public class NickTool extends CommandTool {

    public NickTool() {
        super("/nick", "nick", LobbyConfig.PERM_TOOL + ".nick", new ItemBuilder().setMaterial(Material.NAME_TAG).setName("§5Nick §8» §7Rechtsklick").build(), 6, 500);
    }

}
