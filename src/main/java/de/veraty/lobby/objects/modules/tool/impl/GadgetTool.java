package de.veraty.lobby.objects.modules.tool.impl;

import de.veraty.lobby.objects.config.LobbyConfig;
import de.veraty.lobby.objects.modules.tool.LobbyTool;
import de.veraty.lobby.objects.tools.ItemBuilder;
import de.veraty.lobby.objects.user.User;
import org.bukkit.Material;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class GadgetTool extends LobbyTool  {

    private ItemStack emptyItemStack;

    public GadgetTool() {
        super("gadget", LobbyConfig.PERM_TOOL + ".gadget", 4, 500);
        this.emptyItemStack = new ItemBuilder().setMaterial(Material.BARRIER).setName("§cKein Gadget ausgewählt §8» §7Rechtsklick").build();
    }

    @Override
    public ItemStack getItemStack(User user) {
        return user.getEquippedGadget() == null ? emptyItemStack : user.getEquippedGadget().getItemStack(user);
    }

    @Override
    public boolean matches(ItemStack other) {
        String displayName = other.getItemMeta().getDisplayName().toLowerCase();
        if (displayName.contains("gadget")) {
            return true;
        }
        return false;
    }

    @Override
    public void onUse(PlayerInteractEvent event) {
        User user = User.getInstance(event.getPlayer());
        if (event.getItem().getItemMeta().getDisplayName().equalsIgnoreCase(emptyItemStack.getItemMeta().getDisplayName())) {

        } else {
            user.getEquippedGadget().onUse(event);
        }
    }
}
