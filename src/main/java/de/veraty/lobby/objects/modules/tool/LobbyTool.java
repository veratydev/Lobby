package de.veraty.lobby.objects.modules.tool;

import lombok.Getter;
import org.bukkit.entity.Player;

@Getter
public abstract class LobbyTool implements Tool {

    private String name, permission;
    private int slot;
    private long cooldown;

    public LobbyTool(String name, String permission, int slot, long cooldown) {
        this.name = name;
        this.permission = permission;
        this.cooldown = cooldown;
        this.slot = slot;

        registerInteractable();
    }

    @Override
    public boolean isPermitted(Player player) {
        return player.hasPermission(permission);
    }

}
