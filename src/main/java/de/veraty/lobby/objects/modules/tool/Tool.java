package de.veraty.lobby.objects.modules.tool;

import de.veraty.lobby.objects.tools.Interactable;
import org.bukkit.entity.Player;

public interface Tool extends Interactable {

    String getName();

    int getSlot();

    boolean isPermitted(Player player);

}
