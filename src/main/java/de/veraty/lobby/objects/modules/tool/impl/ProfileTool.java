package de.veraty.lobby.objects.modules.tool.impl;

import de.veraty.lobby.objects.config.LobbyConfig;
import de.veraty.lobby.objects.modules.tool.LobbyTool;
import de.veraty.lobby.objects.user.User;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class ProfileTool extends LobbyTool {

    public static String DISPLAY_NAME = "§dDein Profil §8» §7Rechtsklick";

    public ProfileTool() {
        super("profile", LobbyConfig.PERM_TOOL + ".profile", 8, 500);
    }

    @Override
    public ItemStack getItemStack(User user) {
        return user.getProfileItem();
    }

    @Override
    public void onUse(PlayerInteractEvent event) {

    }

    @Override
    public boolean matches(ItemStack other) {
        return other.getItemMeta().getDisplayName().equalsIgnoreCase(DISPLAY_NAME);
    }
}
