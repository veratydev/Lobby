package de.veraty.lobby.objects.modules.tool.impl;

import de.veraty.lobby.objects.config.LobbyConfig;
import de.veraty.lobby.objects.modules.tool.LobbyTool;
import de.veraty.lobby.objects.tools.InventoryOpener;
import de.veraty.lobby.objects.tools.ItemBuilder;
import de.veraty.lobby.objects.user.User;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class VanishTool extends LobbyTool implements InventoryOpener {

    private static final String TITLE = "§7§lSichtbarkeit", DISPLAY_NAME = "%sSichtbarkeit §8» §7Rechtsklick";

    public VanishTool() {
        super("vanish", LobbyConfig.PERM_TOOL + ".vanish", 1, 500);
        registerOpener();
    }

    @Override
    public Inventory getInventory(User user) {
        Inventory inventory = Bukkit.createInventory(null, 9 * 5, TITLE);
        inventory.setItem(toSlot(new int[]{1, 4}), user.getVisibilityLevel().getBlockItem());
        inventory.setItem(toSlot(new int[]{3, 3}), VisibilityLevel.ALL.getButtonItem());
        inventory.setItem(toSlot(new int[]{3, 4}), VisibilityLevel.VIP.getButtonItem());
        inventory.setItem(toSlot(new int[]{3, 5}), VisibilityLevel.NONE.getButtonItem());
        return inventory;
    }

    @Override
    public ItemStack getItemStack(User user) {
        return new ItemBuilder()
                .setMaterial(Material.INK_SACK)
                .setName(String.format(DISPLAY_NAME, user.getVisibilityLevel().getChatColor()))
                .setData(user.getVisibilityLevel().getInkData())
                .build();
    }


    @Override
    public void onClick(InventoryClickEvent event) {
        User user = User.getInstance((Player) event.getWhoClicked());
        for (VisibilityLevel visibilityLevel : VisibilityLevel.values()) {
            if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(visibilityLevel.getDisplayName())) {
                if (user.getVisibilityLevel() != visibilityLevel) {
                    user.setVisibilityLevel(visibilityLevel);
                    user.updateVisbility();
                    user.play(Sound.ITEM_PICKUP);
                    event.getInventory().setItem(toSlot(new int[]{1, 4}), user.getVisibilityLevel().getBlockItem());
                    user.getPlayer().getInventory().setItem(getSlot(), getItemStack(user));
                }
            }
        }
    }

    @Override
    public void onUse(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR) {
            event.getPlayer().openInventory(getInventory(User.getInstance(event.getPlayer())));
        }
    }

    @Override
    public boolean matches(Inventory other) {
        return other.getTitle().equalsIgnoreCase(TITLE);
    }

    @Override
    public boolean matches(ItemStack other) {
        return other.getItemMeta().getDisplayName().contains("Sichtbarkeit");
    }

    @Getter
    public enum VisibilityLevel {

        ALL("", "§a", "Alle", (byte) 5, (byte) 10),
        VIP("lobby.visbility.vip", "§5", "VIP's und Teammitglieder", (byte) 10, (byte) 5),
        NONE(null, "§c", "Niemanden", (byte) 14, (byte) 1);

        private String permission, displayName, chatColor;
        private ItemStack buttonItem, blockItem;
        private byte dyeData;
        private byte inkData;

        VisibilityLevel(String permission, String chatColor, String displayName, byte dyeData, byte inkData) {
            this.permission = permission;
            this.chatColor = chatColor;
            this.displayName = chatColor + displayName;
            this.inkData = inkData;
            this.dyeData = dyeData;
            this.buttonItem = new ItemBuilder().setMaterial(Material.INK_SACK).setData(inkData).setName(this.displayName).build();
            this.blockItem = new ItemBuilder().setMaterial(Material.WOOL).setData(dyeData).setName(this.displayName).build();

        }

        public boolean canBeSeen(Player player) {
            if (permission == null)
                return false;
            return permission.equalsIgnoreCase("") ? true : player.hasPermission(permission);
        }

    }

}
