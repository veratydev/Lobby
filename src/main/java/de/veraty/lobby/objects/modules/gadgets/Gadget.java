package de.veraty.lobby.objects.modules.gadgets;

import de.veraty.lobby.objects.tools.Interactable;

public interface Gadget extends Interactable {

    String getName();

    /**
     * @return price in coins
     */
    int getPrice();

}
