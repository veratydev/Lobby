package de.veraty.lobby.objects.modules.tool.impl;

import de.veraty.lobby.LobbyPlugin;
import de.veraty.lobby.objects.config.LobbyConfig;
import de.veraty.lobby.objects.modules.tool.LobbyTool;
import de.veraty.lobby.objects.modules.warp.Warp;
import de.veraty.lobby.objects.modules.warp.impl.SpawnWarp;
import de.veraty.lobby.objects.tools.InventoryOpener;
import de.veraty.lobby.objects.tools.ItemBuilder;
import de.veraty.lobby.objects.user.User;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class TeleporterTool extends LobbyTool implements InventoryOpener {

    private static final String TITLE = "§e§lNavigator";
    private static final int[][] usedSlots;

    static {
        usedSlots = new int[][]{
                {3, 2},
                {3, 3},
                {3, 5},
                {3, 6},
                {3, 4},
                {4, 2},
                {4, 3},
                {4, 4},
                {4, 5},
                {4, 6}

        };
    }

    private Inventory teleporterInventory;
    private ItemStack itemStack;

    public TeleporterTool() {
        super("teleport", LobbyConfig.PERM_TOOL + ".teleport", 0, 1000);
        this.itemStack = new ItemBuilder().setMaterial(Material.COMPASS).setName("§eNavigator §8» §7Rechtsklick").build();

        this.teleporterInventory = Bukkit.createInventory(null, 9 * 5, TITLE);

        int warps = LobbyPlugin.getInstance().getWarps().size();
        int slots = (int) (Math.ceil(warps / 4f)) * 4;

        teleporterInventory.setItem(toSlot(new int[]{1, 4}), SpawnWarp.getInstance().getItemStack());

        for (int i = 0; i < slots; i++) {
            if (usedSlots.length > i) {
                if (warps > i) {
                    teleporterInventory.setItem(toSlot(usedSlots[i]), LobbyPlugin.getInstance().getWarps().get(i).getItemStack());
                } else {
                    teleporterInventory.setItem(toSlot(usedSlots[i]), ItemBuilder.NULL);
                }
            }
        }

        registerOpener();
    }

    @Override
    public ItemStack getItemStack(User user) {
        return itemStack;
    }

    @Override
    public Inventory getInventory(User user) {
        return this.teleporterInventory;
    }

    @Override
    public boolean matches(ItemStack other) {
        return other.getItemMeta().getDisplayName().equalsIgnoreCase(getItemStack(null).getItemMeta().getDisplayName());
    }

    @Override
    public boolean matches(Inventory other) {
        return other.getTitle().equalsIgnoreCase(getInventory(null).getTitle());
    }

    @Override
    public void onUse(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR) {
            event.getPlayer().openInventory(getInventory(null));
        }
    }

    @Override
    public void onClick(InventoryClickEvent event) {
        User user = User.getInstance((Player) event.getWhoClicked());
        String displayName = event.getCurrentItem().getItemMeta().getDisplayName();
        Warp clickedWarp = null;

        if (displayName.equalsIgnoreCase(SpawnWarp.getInstance().getItemStack().getItemMeta().getDisplayName())) {
            clickedWarp = SpawnWarp.getInstance();
        } else {
            for (Warp warp : LobbyPlugin.getInstance().getWarps()) {
                if (warp.getItemStack().getItemMeta().getDisplayName().equalsIgnoreCase(displayName)) {
                    clickedWarp = warp;
                }
            }
        }

        if (clickedWarp != null) {
            event.getWhoClicked().teleport(clickedWarp.getLocation());
            user.play(Sound.ENDERMAN_TELEPORT);
            user.showActionBar("Du wurdest zum Punkt §<%s§> teleportiert", displayName);
        }
    }

}
