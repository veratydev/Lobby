package de.veraty.lobby.objects.modules.tool;

import de.veraty.lobby.objects.user.User;
import lombok.Getter;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

@Getter
public class CommandTool extends LobbyTool {

    private String command;
    private ItemStack itemStack;

    public CommandTool(String command, String name, String permission, ItemStack itemStack, int slot, long cooldown) {
        super(name, permission, slot, cooldown);
        this.command = command;
        this.itemStack = itemStack;
    }

    @Override
    public ItemStack getItemStack(User user) {
        return itemStack;
    }

    @Override
    public boolean matches(ItemStack other) {
        return itemStack.getItemMeta().getDisplayName().equalsIgnoreCase(other.getItemMeta().getDisplayName());
    }

    @Override
    public void onUse(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            event.getPlayer().performCommand(command);
        }
    }


}
