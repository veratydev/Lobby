package de.veraty.lobby.objects.modules.warp;

import lombok.Getter;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public interface Warp {

   boolean isPermitted(Player player);

   Location getLocation();

   ItemStack getItemStack();

}
