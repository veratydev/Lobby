package de.veraty.lobby.objects.modules.warp.impl;

import de.veraty.lobby.objects.modules.warp.SimpleWarp;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Material;

public class SpawnWarp extends SimpleWarp {

    public static @Getter SpawnWarp instance;

    public SpawnWarp() {
        super("&a&lSpawn", "", Bukkit.getWorlds().get(0).getSpawnLocation(), Material.EYE_OF_ENDER.getId());
        instance = this;
    }

}
