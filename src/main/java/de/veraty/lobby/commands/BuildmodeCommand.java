package de.veraty.lobby.commands;

import de.veraty.lobby.LobbyPlugin;
import de.veraty.lobby.objects.chat.ChatFormatter;
import de.veraty.lobby.objects.config.LobbyConfig;
import de.veraty.lobby.objects.user.User;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class BuildmodeCommand extends BukkitCommand {

    private String permission;

    public BuildmodeCommand() {
        super("buildmode", "Erlaubt/Verbietet Leuten zu bauen", "buildmode <true/false>", Arrays.asList("build", "bm"));
        this.permission = LobbyConfig.PERM_COMMAND + "." + getName();
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {

        if (sender instanceof Player) {
            if (sender.hasPermission(permission)) {
                User user = User.getInstance((Player) sender);
                if (user.isBuildingAllowed()) {
                    sender.sendMessage(ChatFormatter.format("Du darfst nun nichtmehr bauen"));
                    user.setBuildingAllowed(false);
                } else {
                    sender.sendMessage(ChatFormatter.format("Du darfst nun bauen"));
                    user.setBuildingAllowed(true);
                }

                user.flush();
                user.updateInventory();

            } else {
                sender.sendMessage(LobbyPlugin.getInstance().getLobbyConfig().getNoPermissionsError());
            }
        } else {
            sender.sendMessage(LobbyPlugin.getInstance().getLobbyConfig().getNoPlayerError());
        }


        return true;
    }
}
