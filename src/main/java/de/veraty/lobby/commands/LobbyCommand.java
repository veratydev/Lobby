package de.veraty.lobby.commands;

import de.veraty.lobby.LobbyPlugin;
import de.veraty.lobby.objects.chat.ChatFormatter;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.event.Listener;

import java.util.Arrays;
import java.util.List;

public class LobbyCommand extends BukkitCommand implements Listener {

    private String version, author;

    public LobbyCommand() {
        super("lobby", "Lobby Command", "lobby", Arrays.asList("lobbyplugin"));
        String version = LobbyPlugin.getInstance().getDescription().getVersion();

        if (version != null) {
            this.version = version;
        } else {
            this.version = "v1";
        }

        List<String> authors = LobbyPlugin.getInstance().getDescription().getAuthors();
        if (authors != null && authors.size() > 0) {
            this.author = authors.get(0);
        } else {
            this.author = "Unbekannt";
        }
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        sender.sendMessage(ChatFormatter.format("Lobby-System §<%s§> §8┃ §<%s", version, author));
        return true;
    }
}
