package de.veraty.lobby;

import de.veraty.lobby.commands.BuildmodeCommand;
import de.veraty.lobby.commands.LobbyCommand;
import de.veraty.lobby.listener.block.*;
import de.veraty.lobby.listener.entity.*;
import de.veraty.lobby.listener.inventory.InventoryClickListener;
import de.veraty.lobby.listener.player.*;
import de.veraty.lobby.listener.weather.LightningStrikeListener;
import de.veraty.lobby.listener.weather.ThunderChangeListener;
import de.veraty.lobby.listener.weather.WeatherChangeListener;
import de.veraty.lobby.listener.word.StructureGrowListener;
import de.veraty.lobby.objects.config.LobbyConfig;
import de.veraty.lobby.objects.modules.ModuleLoader;
import de.veraty.lobby.objects.modules.tool.Tool;
import de.veraty.lobby.objects.modules.warp.SimpleWarp;
import de.veraty.lobby.objects.modules.warp.Warp;
import de.veraty.lobby.objects.modules.warp.impl.SpawnWarp;
import de.veraty.lobby.objects.tools.Interactable;
import de.veraty.lobby.objects.tools.InventoryOpener;
import de.veraty.lobby.objects.user.User;
import de.veraty.lobby.utils.ReflectionUtils;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.CommandMap;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Getter
public class LobbyPlugin extends JavaPlugin {

    private static @Getter LobbyPlugin instance;
    private static @Getter int version;

    static {
        version = 0;
    }

    private ModuleLoader moduleLoader;
    private LobbyConfig lobbyConfig;
    private Collection<Tool> registeredTools;


    private List<Warp> warps;
    private List<Interactable> interactables;
    private List<InventoryOpener> inventoryOpeners;

    public LobbyPlugin() {
        instance = this;

        this.interactables = new ArrayList<>();
        this.inventoryOpeners = new ArrayList<>();
        this.registeredTools = new ArrayList<>();
        this.warps = new ArrayList<>();
    }

    @Override
    public void onEnable() {

        register();

        loadModules();

        createDefaults();

        createConfig();

    }

    private void loadModules() {
        try {
            File moduleFile = new File(getDataFolder(), "modules");
            if (!moduleFile.exists()) {
                moduleFile.mkdirs();
            }
            this.moduleLoader = new ModuleLoader(moduleFile);
            moduleLoader.loadModules();
        } catch (IOException | ClassNotFoundException | InstantiationException | IllegalAccessException exception) {
            exception.printStackTrace();
        }
    }

    private void createDefaults() {
        new SpawnWarp();
        this.warps.add(new SimpleWarp("&7&lKnockbackFFA", "", Bukkit.getWorlds().get(0).getSpawnLocation(), Material.SANDSTONE.getId()));
        this.warps.add(new SimpleWarp("&c&lBedwars", "", Bukkit.getWorlds().get(0).getSpawnLocation(), Material.BED.getId()));
        this.warps.add(new SimpleWarp("&2&lSkywars", "", Bukkit.getWorlds().get(0).getSpawnLocation(), Material.GRASS.getId()));
        this.warps.add(new SimpleWarp("&3&lQSG", "", Bukkit.getWorlds().get(0).getSpawnLocation(), Material.IRON_SWORD.getId()));
    }

    private void createConfig() {
        lobbyConfig = new LobbyConfig();
        getConfig().options().copyDefaults(true);
        saveConfig();
        readConfig();
    }

    public void readConfig() {
        lobbyConfig.load(getConfig());
    }

    private void register() {

        ReflectionUtils.getServerVersion(getServer());

        registerCommands(new BukkitCommand[]{
                new BuildmodeCommand(),
                new LobbyCommand()
        });

        registerListener(new Listener[]{
                new BlockBreakListener(),
                new BlockBurnListener(),
                new BlockDamageListener(),
                new BlockDispenseListener(),
                new BlockExplodeListener(),
                new BlockFadeListener(),
                new BlockFormListener(),
                new BlockFromToListener(),
                new BlockGrowListener(),
                new BlockIgniteListener(),
                new BlockPlaceListener(),
                new BlockSpreadListener(),
                new EntityBlockFormListener(),
                new LeavesDecayListener(),

                new CreatureSpawnListener(),
                new EntityBreakDoorListener(),
                new EntityChangeBlockListener(),
                new EntityCreatePortalListener(),
                new EntityDamageListener(),
                new EntityExplodeListener(),
                new FoodLevelChangeListener(),
                new PlayerDeathListener(),
                new PotionSplashListener(),

                new InventoryClickListener(),

                new AsyncPlayerChatListener(),
                new PlayerAchievementAwardedListener(),
                new PlayerArmorStandManipulateListener(),
                new PlayerDropItemListener(),
                new PlayerInteractListener(),
                new PlayerJoinListener(),
                new PlayerKickListener(),
                new PlayerPickupItemListener(),
                new PlayerPortalListener(),
                new PlayerQuitListener(),
                new PlayerShearEntityListener(),
                new PlayerStatisticsIncrementListener(),
                new PlayerToggleFlightListener(),

                new LightningStrikeListener(),
                new ThunderChangeListener(),
                new WeatherChangeListener(),

                new StructureGrowListener()
        });
    }

    private void registerCommands(BukkitCommand[] bukkitCommands) {

        try {
            CommandMap commandMap = (CommandMap) ReflectionUtils.getCommandMap(getServer());
            for (BukkitCommand command : bukkitCommands) {
                commandMap.register(command.getName(), command);
            }
        } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException exception) {
            exception.printStackTrace();
        }

    }

    private void registerListener(Listener[] listeners) {
        for (Listener listener : listeners) {
            Bukkit.getPluginManager().registerEvents(listener, this);
        }
    }

    public Interactable getInteractable(ItemStack itemStack) {
        for (Interactable interactable : interactables) {
            if (interactable.matches(itemStack)) {
                return interactable;
            }
        }
        return null;
    }

    public InventoryOpener getInventoryOpener(Inventory inventory) {
        for (InventoryOpener inventoryOpener : inventoryOpeners) {
            if (inventoryOpener.matches(inventory)) {
                return inventoryOpener;
            }
        }
        return null;
    }

    public static void broadcastChatMessage(String message, Object... args) {
        String string = String.format(message, args);
        for (Player player : Bukkit.getOnlinePlayers()) {
            User user = User.getInstance(player);
            if (!user.isSilentLobby()) {
                user.getPlayer().sendMessage(string);
            }
        }
        Bukkit.getConsoleSender().sendMessage(string);
    }

}

